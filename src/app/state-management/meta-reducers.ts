import { storageSync } from '@larscom/ngrx-store-storagesync'
import { ActionReducer, MetaReducer } from '@ngrx/store'

export function storageSyncReducer<T>(reducer: ActionReducer<T>) {

  const sync = storageSync<T>({
    features: [
      { stateKey: 'league', serialize: (featureState: Partial<T>) => JSON.stringify(featureState), deserialize: (featureState: string) => JSON.parse(featureState) },
      { stateKey: 'team', serialize: (featureState: Partial<T>) => JSON.stringify(featureState), deserialize: (featureState: string) => JSON.parse(featureState) },
      { stateKey: 'player', serialize: (featureState: Partial<T>) => JSON.stringify(featureState), deserialize: (featureState: string) => JSON.parse(featureState) }
    ],
    storage: window.localStorage
  })

  return sync(reducer)
}

export const metaReducers: MetaReducer[] = [storageSyncReducer]
