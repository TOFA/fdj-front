import { RouterReducerState } from '@ngrx/router-store'
import { LeagueState } from './league-store/state'
import { PlayerState } from './player-store/state'
import { TeamState } from './team-store/state'




export interface State {
    router: RouterReducerState
    league: LeagueState
    team: TeamState
    player: PlayerState
  }
