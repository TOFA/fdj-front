import * as fromRouter from '@ngrx/router-store'
import { createFeatureSelector, createSelector } from '@ngrx/store'

export const selectRouter = createFeatureSelector<fromRouter.RouterReducerState>('router')

export const selectRouterStateRoot = createSelector(
  selectRouter,
  (routerState: fromRouter.RouterReducerState) => routerState.state.root
)
