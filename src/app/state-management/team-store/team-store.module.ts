import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { EffectsModule } from '@ngrx/effects'
import { StoreModule } from '@ngrx/store'
import { TeamStoreEffects } from './effects'
import { teamReducer } from './reducer'




@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('team', teamReducer),
    EffectsModule.forFeature([TeamStoreEffects])
  ]
})
export class TeamStoreModule { }
