import { Action, createReducer, on } from '@ngrx/store'
import * as teamActions from './actions'
import { initialState, TeamState } from './state'

const reducer = createReducer(

  initialState,

  on(teamActions.LoadTeams, (state) => ({ ...state, areLoading: true })),
  on(teamActions.LoadTeamsSuccessful, (state, { teams }) => ({ ...state, teams, areLoading: false })),
  on(teamActions.LoadTeamsFailed, (state) => ({ ...state, areLoading: false }))

)

export const teamReducer = (state: TeamState | undefined, action: Action) => reducer(state, action)
