import { createAction, props } from '@ngrx/store'
import { ITeam } from '../../shared/model/team'

export const LoadTeams = createAction('[Teams] Loading Teams ',props<{ ids: string[] }>())
export const LoadTeamsSuccessful = createAction('[Teams] Loaded Teams  successfully', props<{ teams: ITeam[] }>())
export const LoadTeamsFailed = createAction('[Teams] Failed to load Teams')
