import { Injectable } from '@angular/core'
import { Actions, createEffect, ofType } from '@ngrx/effects'
import { of } from 'rxjs'
import { catchError, flatMap, map } from 'rxjs/operators'
import { ApiService } from '../../shared/services/api.service'
import { LoadTeams, LoadTeamsFailed, LoadTeamsSuccessful } from './actions'



@Injectable()

export class TeamStoreEffects {

  constructor(
    private actions: Actions,
    private apiService: ApiService
  ) { }

  LoadTeams$ = createEffect(() =>
    this.actions.pipe(
      ofType(LoadTeams),
      flatMap(({ ids }) => this.getTeams(ids))
    )
  )

  getTeams = (ids: string []) => this.apiService.getTeamsByLeague(ids).pipe(
    map((teams: []) => LoadTeamsSuccessful({ teams })),
    catchError(_ => of(LoadTeamsFailed))
  )
}
