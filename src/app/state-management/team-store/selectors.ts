import { createFeatureSelector, createSelector } from '@ngrx/store'
import { TeamState } from './state'

const getTeams = (teamState: TeamState) => teamState.teams

const getLoadingStatus = (teamState: TeamState) => teamState.areLoading

export const selectTeamsState = createFeatureSelector<TeamState>(
  'team'
)

export const areTeamsLoading = createSelector(
  selectTeamsState,
  getLoadingStatus
)

export const selectTeams = createSelector(
  selectTeamsState,
  getTeams
)
