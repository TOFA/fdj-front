export interface TeamState {
  teams: []
  areLoading: boolean
}

export const initialState: TeamState = {
  teams: [],
  areLoading: false
}
