import { Injectable } from '@angular/core'
import { Actions, createEffect, ofType } from '@ngrx/effects'
import { of } from 'rxjs'
import { catchError, flatMap, map } from 'rxjs/operators'
import { ApiService } from '../../shared/services/api.service'
import { LoadLeagues, LoadLeaguesFailed, LoadLeaguesSuccessful } from './actions'

@Injectable()

export class LeagueStoreEffects {

  constructor(
    private actions: Actions,
    private apiService: ApiService
  ) { }

  LoadLeagues$ = createEffect(() =>
    this.actions.pipe(
      ofType(LoadLeagues),
      flatMap(() => this.getLeagues())
    )
  )

  getLeagues = () => this.apiService.getLeagues().pipe(
    map((leagues: []) => LoadLeaguesSuccessful({ leagues })),
    catchError(_ => of(LoadLeaguesFailed))
  )

}
