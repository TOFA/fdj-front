import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { EffectsModule } from '@ngrx/effects'
import { StoreModule } from '@ngrx/store'
import { LeagueStoreEffects } from './effects'
import { leagueReducer } from './reducer'




@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('league', leagueReducer),
    EffectsModule.forFeature([LeagueStoreEffects])
  ]
})
export class LeagueStoreModule { }
