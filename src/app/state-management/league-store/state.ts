export interface LeagueState {
  leagues: []
  areLoading: boolean
}

export const initialState: LeagueState = {
  leagues: [],
  areLoading: false
}
