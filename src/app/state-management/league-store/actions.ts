import { createAction, props } from '@ngrx/store'
import { ILeague } from '../../shared/model/league'

export const LoadLeagues = createAction('[Leagues] Loading Leagues ')
export const LoadLeaguesSuccessful = createAction('[Leagues] Loaded Leagues  successfully', props<{ leagues: ILeague[] }>())
export const LoadLeaguesFailed = createAction('[Leagues] Failed to load Leagues')

