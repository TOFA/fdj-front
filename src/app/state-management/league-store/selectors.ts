import { createFeatureSelector, createSelector } from '@ngrx/store'
import { LeagueState } from './state'

const getLeagues = (leaguesState: LeagueState) => leaguesState.leagues

const getLoadingStatus = (leaguesState: LeagueState) => {
  return leaguesState.areLoading
}

export const selectLeagueState = createFeatureSelector<LeagueState>(
  'league'
)

export const areLeaguesLoading = createSelector(
  selectLeagueState,
  getLoadingStatus
)

export const selectLeagues = createSelector(
  selectLeagueState,
  getLeagues
)
