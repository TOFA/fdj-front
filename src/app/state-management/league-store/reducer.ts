import { Action, createReducer, on } from '@ngrx/store'
import * as leagueActions from './actions'
import { initialState, LeagueState } from './state'

const reducer = createReducer(

  initialState,

  on(leagueActions.LoadLeagues, (state) => ({ ...state, areLoading: true })),
  on(leagueActions.LoadLeaguesSuccessful, (state, { leagues }) => ({ ...state, leagues, areLoading: false })),
  on(leagueActions.LoadLeaguesFailed, (state) => ({ ...state, areLoading: false }))

)

export const leagueReducer = (state: LeagueState | undefined, action: Action) => reducer(state, action)
