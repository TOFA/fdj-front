import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { EffectsModule } from '@ngrx/effects'
import { StoreModule } from '@ngrx/store'
import { PlayerStoreEffects } from './effects'
import { playerReducer } from './reducer'

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('player', playerReducer),
    EffectsModule.forFeature([PlayerStoreEffects])
  ]
})
export class PlayerStoreModule { }
