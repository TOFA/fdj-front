import { createAction, props } from '@ngrx/store'
import { IPlayer } from '../../shared/model/player'
import { ITeam } from '../../shared/model/team'
export const LoadPlayers = createAction('[Player] Loading Player ',props<{ ids: string[] }>())
export const LoadPlayersSuccessful = createAction('[Player] Loaded Player  successfully', props<{ players: IPlayer[] }>())
export const LoadPlayersFailed = createAction('[Player] Failed to load Player')

export const SetPlayersTeam = createAction('[Player] Set current box', props<{team: ITeam}>())
