import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Actions, createEffect, ofType } from '@ngrx/effects'
import { of } from 'rxjs'
import { catchError, flatMap, map } from 'rxjs/operators'
import { IPlayer } from '../../shared/model/player'
import { ApiService } from '../../shared/services/api.service'
import { LoadPlayers, LoadPlayersFailed, LoadPlayersSuccessful } from './actions'



@Injectable()

export class PlayerStoreEffects {

  constructor(
    private actions: Actions,
    private apiService: ApiService,
    private router: Router
  ) { }

  LoadPlayers$ = createEffect(() =>
    this.actions.pipe(
      ofType(LoadPlayers),
      flatMap(({ ids }) => {
        this.router.navigate(['/player'])
        return this.getPlayers(ids,LoadPlayersSuccessful,LoadPlayersFailed)
      })
    )
  )

  private getPlayers = (
    ids: string[],
    successAction: typeof LoadPlayersSuccessful ,
    failedAction: typeof LoadPlayersFailed
  ) =>
    this.apiService.getPlayersByTeam(ids).pipe(
      map((players: IPlayer[]) => successAction({ players })),
      catchError(_ => of(failedAction()))
    )

}
