import { ITeam } from '../../shared/model/team'

export interface PlayerState {
  players: []

  team: ITeam
  areLoading: boolean
}

export const initialState: PlayerState = {
  players: [],
  team: null,
  areLoading: false
}
