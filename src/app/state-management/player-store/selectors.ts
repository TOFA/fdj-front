import { createFeatureSelector, createSelector } from '@ngrx/store'
import { PlayerState } from './state'

const getPlayers = (playerState: PlayerState) => playerState.players

const getLoadingStatus = (playerState: PlayerState) => playerState.areLoading

const getPlayersTeam = (playerState: PlayerState) => playerState.team.name



export const selectPlayerState = createFeatureSelector<PlayerState>(
  'player'
)

export const arePlayersLoading = createSelector(
  selectPlayerState,
  getLoadingStatus
)

export const selectPlayers = createSelector(
  selectPlayerState,
  getPlayers
)

export const selectPlayersTeam = createSelector(
  selectPlayerState,
  getPlayersTeam
)
