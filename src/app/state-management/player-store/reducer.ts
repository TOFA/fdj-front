import { Action, createReducer, on } from '@ngrx/store'
import * as playerActions from './actions'
import { initialState, PlayerState } from './state'

const reducer = createReducer(

  initialState,

  on(playerActions.LoadPlayers, (state) => ({ ...state, areLoading: true })),
  on(playerActions.LoadPlayersSuccessful, (state, { players }) => ({ ...state, players, areLoading: false })),
  on(playerActions.LoadPlayersFailed, (state) => ({ ...state, areLoading: false })),

  on(playerActions.SetPlayersTeam, (state, { team }) => ({ ...state, team }))

)

export const playerReducer = (state: PlayerState | undefined, action: Action) => reducer(state, action)
