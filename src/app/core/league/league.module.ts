import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { MaterialModule } from '../../shared/material/material.module'
import { SharedModule } from '../../shared/shared.module'
import { LeagueListComponent } from './league-list/league-list.component'
import { LeagueRoutingModule } from './league.routes'



@NgModule({
  declarations: [LeagueListComponent],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    LeagueRoutingModule
  ]
})
export class LeagueModule { }
