import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { LeagueListComponent } from './league-list/league-list.component'



const leagueRoutes: Routes = [
  {
    path: '',
    component: LeagueListComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(leagueRoutes)],
  exports: [RouterModule]
})

export class LeagueRoutingModule { }
