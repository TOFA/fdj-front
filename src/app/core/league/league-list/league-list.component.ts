import { Component, OnDestroy, OnInit } from '@angular/core'
import { FormControl } from '@angular/forms'
import { Store } from '@ngrx/store'
import { Observable, of, Subject } from 'rxjs'
import { map, startWith, takeUntil, tap } from 'rxjs/operators'
import { ILeague } from '../../../shared/model/league'
import { ITeam } from '../../../shared/model/team'
import { RootStoreState } from '../../../state-management'
import { LoadLeagues } from '../../../state-management/league-store/actions'
import { selectLeagues } from '../../../state-management/league-store/selectors'
import { LoadPlayers, SetPlayersTeam } from '../../../state-management/player-store/actions'
import { LoadTeams } from '../../../state-management/team-store/actions'
import { areTeamsLoading, selectTeams } from '../../../state-management/team-store/selectors'


@Component({
  selector: 'fdj-league-list',
  templateUrl: './league-list.component.html',
  styleUrls: ['./league-list.component.scss']
})
export class LeagueListComponent implements OnInit, OnDestroy {

  destroy: Subject<boolean> = new Subject<boolean>()
  areLoading$: Observable<boolean>
  leagues: ILeague[]=[]
  myControl = new FormControl()
  teams$: Observable<ITeam[]>
  filteredLeagues: Observable<ILeague[]>

  constructor(
    private store: Store<RootStoreState.State>
  ) {
    this.store.dispatch(LoadLeagues())
    this.store.select(selectLeagues).pipe(
      takeUntil(this.destroy),
      tap(leagues => this.leagues = leagues)
    ).subscribe()
  }

  ngOnDestroy(): void {
    this.destroy.next(true)
    this.destroy.unsubscribe()
  }

  ngOnInit(): void {
    this.filteredLeagues = this.myControl.valueChanges.pipe(
      startWith(''),
      map(league => league ? this._filter(league) : this.leagues.slice())
    )
  }

  private _filter(value: string): ILeague[] {
    const filterValue = value.toLowerCase()
    return this.leagues.filter(({ name }) => name.toLowerCase().indexOf(filterValue) === 0)
  }

  getTeamsBySelectedLeag(selectedLeague: ILeague): void{
    this.store.dispatch(LoadTeams({ ids: selectedLeague.teams }))
    this.areLoading$ = this.store.select(areTeamsLoading)
    this.teams$ = this.store.select(selectTeams)
  }

  clearSearchField(): void{
    this.myControl.reset()
    this.teams$ =of([])
  }

  goToTeamPlayers(team: ITeam): void{
    this.store.dispatch(LoadPlayers({ ids: team.players }))
    this.store.dispatch(SetPlayersTeam({ team }))
  }




}
