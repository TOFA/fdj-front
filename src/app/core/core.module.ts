import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { MaterialModule } from '../shared/material/material.module'
import { SharedModule } from '../shared/shared.module'
import { CoreRoutingModule } from './core.routes'


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MaterialModule,
    CoreRoutingModule,
    SharedModule
  ]
})
export class CoreModule { }
