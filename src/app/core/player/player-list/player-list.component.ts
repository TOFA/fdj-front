import { Component, OnDestroy, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Store } from '@ngrx/store'
import { Observable, Subject } from 'rxjs'
import { IPlayer } from '../../../shared/model/player'
import { RootStoreState } from '../../../state-management'
import { arePlayersLoading, selectPlayers, selectPlayersTeam } from '../../../state-management/player-store/selectors'

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.scss']
})
export class PlayerListComponent implements OnInit, OnDestroy {

  destroy: Subject<boolean> = new Subject<boolean>()
  players$: Observable<IPlayer[]>
  areLoading$: Observable<boolean>
  teamName$: Observable<string>

  constructor(
    private store: Store<RootStoreState.State>,
    private router: Router
  ) {

    this.players$ = this.store.select(selectPlayers)
    this.teamName$ =this.store.select(selectPlayersTeam)
  }

  ngOnDestroy(): void {
    this.destroy.next(true)
    this.destroy.unsubscribe()
  }

  ngOnInit(): void {
    this.areLoading$ = this.store.select(arePlayersLoading)
  }

  goBack(): void{
    this.router.navigate(['/league'])
  }
}
