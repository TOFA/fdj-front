import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { PlayerListComponent } from './player-list/player-list.component'



const leagueRoutes: Routes = [
  {
    path: '',
    component: PlayerListComponent
  }

]

@NgModule({
  imports: [RouterModule.forChild(leagueRoutes)],
  exports: [RouterModule]
})

export class PlayerRoutingModule { }
