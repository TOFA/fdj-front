import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { MaterialModule } from '../../shared/material/material.module'
import { SharedModule } from '../../shared/shared.module'
import { PlayerListComponent } from './player-list/player-list.component'
import { PlayerRoutingModule } from './player.routes'



@NgModule({
  declarations: [PlayerListComponent],
  imports: [
    CommonModule,
    PlayerRoutingModule,
    MaterialModule,
    SharedModule
  ]
})
export class PlayerModule { }
