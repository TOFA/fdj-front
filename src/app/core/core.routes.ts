
import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

const scenariosRoutes: Routes = [
  {
    path: 'league',
    loadChildren: () =>
      import('./league/league.module').then((m) => m.LeagueModule)
  },
  {
    path: 'player',
    loadChildren: () =>
      import('./player/player.module').then((m) => m.PlayerModule)
  },
  { path: '', redirectTo: 'league', pathMatch: 'full' }
]

@NgModule({
  imports: [RouterModule.forChild(scenariosRoutes)],
  exports: [RouterModule]
})

export class CoreRoutingModule { }
