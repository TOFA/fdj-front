export interface ISignin{
  amount: number
  currency: string
}

export interface IPlayer {
  _id: string
  born: string
  name: string
  position: string
  signin: ISignin
  thumbnail: string
}
