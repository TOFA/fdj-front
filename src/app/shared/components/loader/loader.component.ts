import { animate, style, transition, trigger } from '@angular/animations'
import { Component, Input } from '@angular/core'

@Component({
  selector: 'fdj-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
  animations: [
    trigger('showHide', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('100ms', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        animate('100ms', style({ opacity: 0 }))
      ])
    ])
  ]
})
export class LoaderComponent {

  @Input()
  areLoading = false

  @Input()
  backdrop = true

  @Input()
  diameter = 50

}
