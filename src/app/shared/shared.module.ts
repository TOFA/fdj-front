import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FlexLayoutModule } from '@angular/flex-layout'
import { RouterModule } from '@angular/router'
import { LoaderComponent } from './components/loader/loader.component'
import { MaterialModule } from './material/material.module'
import { ApiService } from './services/api.service'




@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    FlexLayoutModule
  ],
  declarations: [
    LoaderComponent],
  exports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    FlexLayoutModule,
    LoaderComponent
  ],
  providers: [
    ApiService
  ]
})
export class SharedModule { }
