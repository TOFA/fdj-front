import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ILeague } from '../../shared/model/league'
import { IPlayer } from '../../shared/model/player'
import { ITeam } from '../../shared/model/team'
import { environment } from './../../../environments/environment'

@Injectable({
  providedIn: 'root'
})


export class ApiService {

  private apiUrl: string

  constructor(
    private httpClient: HttpClient
  ) {
    this.apiUrl = environment.apiBaseUrl
  }

  getHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  getLeagues(): Observable<ILeague[]> {
    const httpOptions = { headers: this.getHeaders() }
    return this.httpClient.get<ILeague[]>(`${this.apiUrl}leagues`, httpOptions)
  }

  getTeamsByLeague(teamsIdArray: string[]): Observable<ITeam[]> {
    let params = new HttpParams()
    params = params.append('teamsIdArray', JSON.stringify(teamsIdArray))
    const httpOptions = { headers: this.getHeaders(),params }
    return this.httpClient.get<ITeam[]>(`${this.apiUrl}teams`, httpOptions)
  }

  getPlayersByTeam(playersIdArray: string[]): Observable<IPlayer[]> {
    let params = new HttpParams()
    params = params.append('playersIdArray', JSON.stringify(playersIdArray))
    const httpOptions = { headers: this.getHeaders(),params }
    return this.httpClient.get<IPlayer[]>(`${this.apiUrl}players`, httpOptions)
  }

}
